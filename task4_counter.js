"use strict";
// Create a createCounter function that returns one more value from each call and starts at 0
// For example:
// const getCount = createCounter();
// console.log (getCount()); // 0:
// console.log (getCount()); // 1:
// console.log (getCount()); // 2:

function createCounter() {
  let num = 0;
  return () => {
    return num++;
  };
}

const getCount = createCounter();

console.log(getCount());
console.log(getCount());
console.log(getCount());
