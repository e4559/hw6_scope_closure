"use strict";
// Find the average age of users.

const users = [
  {
    gender: "male",
    age: 24,
  },
  {
    gender: "female",
    age: 21,
  },
  {
    gender: "male",
    age: 36,
  },
];

function averageAge(users) {
  const age = users.reduce((acc, currentVal) => {
    acc += currentVal.age;
    return acc;
  }, 0);
  
  return age / users.length;
}

console.log(averageAge(users));
