"use strict";
// Write a function that gets an array and returns unique values with a new array

// const result = findUniqueElements ([10, 5, 6, 10, 6, 7]);
// result = [5, 7]

function findUniqueElements(arr) {
  return arr.filter((el) => arr.indexOf(el) === arr.lastIndexOf(el));
}

console.log(findUniqueElements([10, 5, 6, 10, 6, 7]));
