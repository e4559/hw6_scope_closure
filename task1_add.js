"use strict";
//Write the function add(3)(6), which returns the sum of these two numbers

function add(x) {
  return (y) => {
    return x + y;
  };
}

console.log(add(3)(6));
